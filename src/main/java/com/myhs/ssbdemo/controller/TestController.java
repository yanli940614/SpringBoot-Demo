package com.myhs.ssbdemo.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableAutoConfiguration
public class TestController {
	
	
	@RequestMapping("/getString")
	public String getString() {
		return "welocome to SpringBoot word //";
	}
	
	public static  void main(String[] args) {
		SpringApplication.run(TestController.class, args);
	}

}
